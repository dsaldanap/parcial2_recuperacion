<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ReportDAO.php';

class Report{
    private $id_report;
    private  $date;
    private  $new_cases;
    private  $cumulative_cases;
    private  $new_deaths;
    private  $cumulative_deaths;
    private  $id_country_country;
    private $conexion;

    private $reporteDAO;

    public function getIdReport()
    {
        return $this->id_report;
    }

 
    public function getDate()
    {
        return $this->date;
    }

    public function getNewCases()
    {
        return $this->new_cases;
    }
    public function getCumulativeCases()
    {
        return $this->cumulative_cases;
    }
    public function getNewDeaths()
    {
        return $this->new_deaths;
    }
    public function getCumulativeDeaths()
    {
        return $this->cumulative_deaths;
    }
    public function getId_country_country()
    {
        return $this->id_country_country;
    }

    

    public function __construct($id_report="", $date="",  $new_cases="",$cumulative_cases="", $new_deaths="",  $cumulative_deaths="",$id_country_country=""){
        $this->id_report=$id_report;
        $this->date=$date;
        $this->new_cases=$new_cases;
        $this->cumulative_cases=$cumulative_cases;
        $this->new_deaths=$new_deaths;
        $this->cumulative_deaths=$cumulative_deaths;
        $this->id_country_country=$id_country_country;
        $this->conexion= new Conexion();
        $this->reporteDAO=new ReportDAO($this->id_report,$this->date, $this->new_cases,$this->cumulative_cases,$this->new_deaths, $this->cumulative_deaths,$this->id_country_country);
        
    }
   
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reporteDAO->consultar());
        
        $this->date=$registro[0];
        $this->new_cases=$registro[1];
        $this->cumulative_cases=$registro[2];
        $this->new_deaths=$registro[3];
        $this->cumulative_deaths=$registro[4];
        $id_country_country = new Country($registro[5]);
        $id_country_country-> consultar();
        $this -> id_country_country = $id_country_country;
        $this->conexion->cerrar();
    }
    
    
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reporteDAO->consultarTodos());
        
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_country_country= new Country($registro[6]);
            $id_country_country->consultar();
            $reporte = new Report($registro[0], $registro[1],$registro[2], $registro[3],$registro[4], $registro[5], $id_country_country);
            array_push($countrys, $country );
        }
        $this -> conexion -> cerrar();
        return  $countrys;
        
        
    } public function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> reporteDAO -> buscar($filtro));
        
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_country_country= new Country($registro[6]);
            $id_country_country->consultar();
            $reporte = new Report($registro[0], $registro[1],$registro[2], $registro[3],$registro[4], $registro[5], $id_country_country);
            array_push($reportes, $reporte );
        }
        $this -> conexion -> cerrar();
        return  $reportes;
    }
    public function buscar2(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> reporteDAO -> buscar2());
        
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_country_country= new Country($registro[6]);
            $id_country_country->consultar();
            $reporte = new Report($registro[0], $registro[1],$registro[2], $registro[3],$registro[4], $registro[5], $id_country_country);
            array_push($reportes, $reporte );
        }
        $this -> conexion -> cerrar();
        return  $reportes;
    }
 
        
    
    
   
}
?>