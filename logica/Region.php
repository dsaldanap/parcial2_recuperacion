<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/RegionDAO.php';
/**
 * @author david
 *
 */
class Region{
    private $id_region;

    private $nombreregion;
    private $conexion;

    private $regionDAO;



    public function getId_region()
    {
        return $this->id_region;
    }
   
    public function getNombreRegion()
    {
        return $this->nombreregion;
    }

  
    public function __construct($id_region="",$nombreregion=""){
        $this->id_region=$id_region;
        $this->nombreregion=$nombreregion;
        $this->conexion= new Conexion();
        $this->regionDAO=new RegionDAO($this->id_region, $this->nombreregion);
        
    }

    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->regionDAO->consultar());
        
        $datos= $this->conexion->extraer();
        $this->nombreregion=$datos[0];
        $this->conexion->cerrar();
       
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> regionDAO -> consultarTodos()); 
        $regiones = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $this->id_region=$registro[0];
            $this->nombreregion=$registro[1];
            $region = new Region($registro[0], $registro[1]); 
            array_push($regiones, $region);
        }
        $this -> conexion -> cerrar();
        return  $regiones;
    }
   
}
?>
