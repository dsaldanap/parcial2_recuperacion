<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/CountryDAO.php';

class Country{
    private $id_country;
    private  $nombre;
    private  $id_region_region;
    private $conexion;

    private $countryDAO;

    public function getIdCountry()
    {
        return $this->id_country;
    }

 
    public function getNombre()
    {
        return $this->nombre;
    }

    public function getId_region_region()
    {
        return $this->id_region_region;
    }

    
    
   

    public function __construct($id_country="", $nombre="",  $id_region_region=""){
        $this->id_country=$id_country;
        $this->nombre=$nombre;
        $this->id_region_region=$id_region_region;
        $this->conexion= new Conexion();
        $this->countryDAO=new CountryDAO($this->id_country,$this->nombre, $this->id_region_region);
        
    }
   
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->countryDAO->consultar());
        $datos= $this->conexion->extraer();
        $this->nombre=$datos[0];
        $id_region_region = new Region($datos[1]);
        $id_region_region -> consultar();
        
        $this -> id_region_region = $id_region_region;
        $this->conexion->cerrar();
    }
    
    
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->countryDAO->consultarTodos());
        
        $countrys = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_region_region= new Region($registro[2]);
            $id_region_region->consultar();
            $country = new Country($registro[0], $registro[1], $id_region_region);
            array_push($countrys, $country );
        }
        $this -> conexion -> cerrar();
        return  $countrys;
        
        
    }
    
 
        
    
    
   
}
?>