<?php 
require_once 'logica/Country.php';
require_once 'logica/Region.php';
require_once 'logica/Report.php';
$country= new Country();
$countrys= $country->consultarTodos();

if(isset($_POST["buscar"])){  
     
    $id_ctr=$_POST["pais"];
    echo $id_ctr;
   $reporte=new Report("","","","","","",$id_ctr);
   $reportes=$reporte->buscar2();
 
}
?>
<html>
<head>
  <link <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" >
  <link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})
</script>
</head>
</head>
<body>
<div class="container">
    <div class="row mt-3">
        <div class="col-4">
        </div>
        <div class="col-4">
            <div class="card">
                <h3 class="card-header text-center">Consultar</h3>
                <div class="card-body">

                    <form method="post" enctype="multipart/form-data">
                       
                        
                        <div class="mb-4 text-center">
                            <label class="form-label">Paises</label>
                            <select class="form-select" name="pais" id="filtro">
                            <?php 
                                foreach ($countrys as $countrysActual)
                                echo "<option  value='".$countrysActual->getIdCountry()."'>".$countrysActual->getNombre()."</option>";
                            ?>
                            </select>
                        </div>
                        <div class="mb-3 text-center">
                            <button type="submit" class="btn btn-primary text-center" name="buscar" >Buscar</button>
                        </div>
                        

                    </form>

                </div>
            </div>
        </div>
        
    </div>
</body>
</html>
<script>

	$("#filtro").change(function(){		
		if($("#filtro").val()!=-1){
			
			var countryCode = $("#filtro").val();
			var path = "index.php?pid=<?php echo base64_encode("mostrar.php")?>="+countryCode;
			$("#searchResult").load(path);
		}else{
			$("#searchResult").html("");
		}
	});

</script> 

<div class="col-12">
</br>
            <div class="card">
                <h3 class="card-header text-center">Resultados</h3>
                <div class="card-body">
    <table class="table table-hover">
         <thead>
             <tr>
                 <th>REGION</th>
                 <th>CODIGO</th>
                 <th>NOMBRE</th>
                 <th>CASOS ACUMULADOS</th>
                 <th>MUERTES ACUMULADAS</th>
                 <th>FECHA</th>
                 
                 
             </tr>
         </thead>
         <tbody>
         <?php foreach ($reportes as $productoActu) {
             ?>
             <tr>
             	 <td><?php echo $productoActu->getId_country_country()->getId_region_region()-> getNombreRegion()?></td>
                 <td><?php echo $productoActu->getId_country_country()->getIdCountry()?></td>
                 <td><?php echo $productoActu->getId_country_country()->getNombre()?></td>
                 <td>
                 <?php echo $productoActu->getCumulativeCases()?>
                    

                     </td>
                 <td><?php echo $productoActu->getCumulativeDeaths();?></td>
                 <td><?php echo $productoActu->getDate();?></td>
                 
             </tr>
            <?php }?>
     
           
            
            
      
           
            
         </tbody>
     </table>

                </div>
            </div>
    </div>
</div>
    
</div>

